#!/bin/bash

mkdir build
cd build
meson ..
ninja
GTK_THEME=Adwaita:dark src/pulseeffects